const hasAccess = require('../server/utils/allowedServices')
const PORT = process.env.PORT || 3001

module.exports = {
    PORT,
    version: process.env.VERSION,
    jwtSecret: process.env.JWT_SECRET,
    pocketloanNotificationService: process.env.POCKETLOAN_NOTIFY,
    pocketloanApi: process.env.POCKETLOAN_API,
    pocketloanTransactions: process.env.POCKETLOAN_TRANSACTIONS,
    pocketloanActivity: process.env.POCKETLOAN_ACTIVITY,
    env: (req) => (req.header.host.includes(`localhost:${PORT}`)) ? 'development' : 'production',
    hasAccess
}