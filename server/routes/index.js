
const GraphQlRouter = require('./graphql')
const schema = require('../schema')

// Exports and Initializes the application routes
module.exports = (app) => {
    const handlers = [
        new GraphQlRouter({ graphiql: true, schema })
    ]

    handlers.forEach(handler => {
        handler.init(app)
    })
}