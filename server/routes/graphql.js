const Router = require('koa-router')
const graphqlHTTP = require('koa-graphql')
const { version } = require('../../config')

class GraphQlRouter extends Router {
    constructor(options = {}) {
        super()
        this.options = options
    }
    // Initializes the application route
    init(app) {
        const { graphiql, schema } = this.options
        this.all('/',
            graphqlHTTP({ 
                schema, 
                graphiql 
            })
        )
        
        app.use(this.routes())
    }
}

module.exports = GraphQlRouter