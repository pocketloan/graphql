const axios = require('../utils/axios')
const { omit, isEmpty } = require('lodash')
const { version, pocketloanTransactions, pocketloanApi, hasAccess } = require('../../config')

class TransactionService {
    constructor(options = {}) {
        this.options = options
        this.access = hasAccess[0]
    }

    async getTransactions() {
        try {
            return axios.get(`${pocketloanTransactions}${version}/transactions`)
                .then(res => res.data.transactions)
                .catch(err => { throw err })
        } catch (err) {

        }
    }

    async getTransaction(id) {
        return axios.get(`${pocketloanTransactions}${version}/transactions/${id}`)
            .then(res => res.data.transaction)
            .catch(err => { throw err })
    }

    async getToken(id) {
        return axios.get(`${pocketloanTransactions}${version}/transactions/tokens/${id}`)
            .then(res => res.data.token)
            .catch(err => { throw err })
    }

    async getCharges() {
        return axios.get(`${pocketloanTransactions}${version}/transactions/charges`)
            .then(res => res.data.charges)
            .catch(err => { throw err })
    }

    async getCustomers() {
        return axios.get(`${pocketloanTransactions}${version}/transactions/customers`)
            .then(res => res.data.customers)
            .catch(err => { throw err })
    }

    async getBalance() {
        return axios.get(`${pocketloanTransactions}${version}/transactions/balance`)
            .then(res => res.data.balance)
            .catch(err => { throw err })
    }

    async getPayouts() {
        return axios.get(`${pocketloanTransactions}${version}/transactions/payouts`)
            .then(res => res.data.payouts)
            .catch(err => { throw err })
    }

    async getAccounts() {
        return axios.get(`${pocketloanTransactions}${version}/transactions/accounts`)
            .then(res => res.data.accounts)
            .catch(err => { throw err })
    }

    async getBalances() {
        return axios.get(`${pocketloanTransactions}${version}/transactions/balances`)
            .then(res => res.data.balances)
            .catch(err => { throw err })
    }

    async getCharge(id) {
        return axios.get(`${pocketloanTransactions}${version}/transactions/charges/${id}`)
            .then(res => res.data.charge)
            .catch(err => { throw err })
    }

    async getCustomer(id) {
        return axios.get(`${pocketloanTransactions}${version}/transactions/customers/${id}`)
            .then(res => res.data.customer)
            .catch(err => { throw err })
    }

    async getPayout(id) {
        return axios.get(`${pocketloanTransactions}${version}/transactions/payouts/${id}`)
            .then(res => res.data.payout)
            .catch(err => { throw err })
    }

    async getAccount(id) {
        return axios.get(`${pocketloanTransactions}${version}/transactions/accounts/${id}`)
            .then(res => res.data.account)
            .catch(err => { throw err })
    }

    async updateTransaction(args) {
        const { userId } = args
        const request = await this._buildUpdateTransaction(omit(args, ['userId']))
        return axios.patch(`${pocketloanTransactions}${version}/transactions/${userId}`, request)
            .then(res => res.data)
            .catch(err => { throw err })
    }

    async updateMandatoryFields(args) {
        const { sub, subType, iss } = this.access
        const { userId, socialInsuranceNumber, accountId, dob, city, state, address, postalCode } = args
        const dateOfBirth = dob.split('/')
        return axios.patch(`${pocketloanApi}${version}/users?sub=${sub}&subType=${subType}&iss=${iss}`, { id: userId, field: { socialInsuranceNumber } })
            .then(res => {
                if (res.data.status === 200) {
                    const document = ''
                    return axios.patch(`${pocketloanTransactions}${version}/transactions/accounts/${accountId}`, {
                        legal_entity: {
                            personal_id_number: socialInsuranceNumber,
                            address: {
                                city,
                                line1: address,
                                postal_code: postalCode,
                                state
                            },
                            verification: {
                                document
                            },
                            dob: {
                                day: dateOfBirth[0],
                                month: dateOfBirth[1],
                                year: dateOfBirth[2]
                            }
                        }
                    }).then(res => res.data).catch(err => { throw err })
                }
            }).catch(err => { throw err })
    }

    async _buildUpdateTransaction(args) {
        let result, index = 0
        if (!isEmpty(args.balance) || !isEmpty(args.customerId)) {
            result = []
            for (let key in args) {
                result[index] = key
                result[index + 1] = args[key]
            }
        }
        if (!isEmpty(args.cardTokens)) {
            result = { client_ip: args.clientIp, created: args.created, cardTokens: { cardTokenId: args.cardTokens } }
        }
        if (!isEmpty(args.bankAccounts)) {
            result = { client_ip: args.clientIp, created: args.created, bankAccounts: { bankAccountTokenId: args.bankAccounts } }
        }
        return result
    }
}

module.exports = TransactionService