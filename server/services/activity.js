const axios = require('../utils/axios')
const { version, pocketloanActivity } = require('../../config')

class ActivityService {
    constructor(options = {}) {
        this.options = options
    }

    async getActivities() {
        return axios.get(`${pocketloanActivity}${version}/activities/`)
            .then(res => res.data.activities)
            .catch(err => { throw err })
    }

    async getActivityById(id) {
        return axios.get(`${pocketloanActivity}${version}/activities/${id}`)
            .then(res => res.data.activity)
            .catch(err => { throw err })
    }

    async getActivityByUserId(id) {
        return axios.get(`${pocketloanActivity}${version}/activities/users/${id}`)
            .then(res => res.data.activity)
            .catch(err => { throw err })
    }

    async getActivityByTransactionId(id) {
        return axios.get(`${pocketloanActivity}${version}/activities/transactions/${id}`)
            .then(res => res.data.activity)
            .catch(err => { throw err })
    }

    async dispatchRequest(params) {
        return axios.post(`${pocketloanActivity}${version}/activities/dispatch`, params)
            .then(res => res.data)
            .catch(err => { throw err })
    }

}

module.exports = ActivityService