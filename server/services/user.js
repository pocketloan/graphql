const axios = require('../utils/axios')
const { isEmpty } = require('lodash')
const jwtDecode = require('jwt-decode')
const { version, pocketloanApi, hasAccess } = require('../../config')

class UserService {
    constructor(options = {}) {
        this.options = options
        this.access = hasAccess[0]
    }

    async getUsers(args) {
        const { sub, subType, iss } = this.access
        const extraQueryParams = await this._buildSearchQuery(args)
        const query = (!isEmpty(extraQueryParams)) ? extraQueryParams : ''
        return axios.get(`${pocketloanApi}${version}/users?sub=${sub}&subType=${subType}&iss=${iss}&${query}`)
            .then(res => res.data.users)
            .catch(err => { throw err })
    }

    async getUser(id) {
        const { sub, subType, iss } = this.access
        return axios.get(`${pocketloanApi}${version}/users/${id}?sub=${sub}&subType=${subType}&iss=${iss}`)
            .then(res => res.data.user)
            .catch(err => { throw err })
    }

    async login({ field, password }) {
        const { sub, subType, iss } = this.access
        return axios.post(`${pocketloanApi}${version}/users/login?sub=${sub}&subType=${subType}&iss=${iss}`, { field, password })
            .then(async (res) => await this._buildLoginResponse(res.data))
            .catch(err => { throw err })
    }

    async updateUser(args) {
        const { sub, subType, iss } = this.access
        const id = args.id
        delete args.id
        return axios.patch(`${pocketloanApi}${version}/users?sub=${sub}&subType=${subType}&iss=${iss}`, { id, field: args })
            .then(res => res.data)
            .catch(err => { throw err })
    }

    async createUser(args) {
        const { sub, subType, iss } = this.access
        return axios.post(`${pocketloanApi}${version}/users?sub=${sub}&subType=${subType}&iss=${iss}`, args)
            .then(res => res.data)
            .catch(err => { throw err })
    }

    async verifyUser({ id }) {
        const { sub, subType, iss } = this.access
        return axios.post(`${pocketloanApi}${version}/users/verify?sub=${sub}&subType=${subType}&iss=${iss}`, { id })
            .then(res => res.data)
            .catch(err => { throw err })
    }

    async _buildLoginResponse(response) {
        const { user } = response
        if (!isEmpty(user)) {
            response.user = await this.decodeJwt(user)
            return response
        }
        return response
    }

    async decodeJwt(token) {
        return jwtDecode(token)
    }

    async _buildSearchQuery(args) {
        let result = ''
        for (let field in args) {
            result += `${field}=${args[field]}&`
        }
        return result.slice(0, -1)
    }
}

module.exports = UserService