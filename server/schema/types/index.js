const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLID,
    GraphQLNonNull,
    GraphQLInt,
    GraphQLList,
    GraphQLBoolean
} = require('graphql')
const GraphQLJSON = require('graphql-type-json')

const TransactionService = require('../../services/transaction')
const UserService = require('../../services/user')
const ActivityService = require('../../services/activity')

const transactionService = new TransactionService()
const userService = new UserService()
const activityService = new ActivityService()

const AccountType = new GraphQLObjectType({
    name: 'AccountType',
    fields: () => ({
        _id: { type: GraphQLNonNull(GraphQLID) },
        accountId: { type: GraphQLString },
        account: {
            type: GraphQLJSON,
            resolve: async ({ accountId }) => {
                const account = await transactionService.getAccount(accountId)
                return account
            }
        }
    })
})

const CardTokenType = new GraphQLObjectType({
    name: 'CardTokenType',
    fields: () => ({
        _id: { type: GraphQLNonNull(GraphQLID) },
        cardTokenId: { type: GraphQLString },
        cardToken: {
            type: GraphQLJSON,
            resolve: async ({ cardTokenId }) => {
                const card = await transactionService.getToken(cardTokenId)
                return card
            }
        }
    })
})

const BankAccountType = new GraphQLObjectType({
    name: 'BankAccountType',
    fields: () => ({
        _id: { type: GraphQLNonNull(GraphQLID) },
        bankAccountTokenId: { type: GraphQLString },
        bankAccount: {
            type: GraphQLJSON,
            resolve: async ({ bankAccountTokenId }) => {
                const account = await transactionService.getBankAccount(bankAccountTokenId)
                return account
            }
        }
    })
})

const PayoutType = new GraphQLObjectType({
    name: 'PayoutType',
    fields: () => ({
        _id: { type: GraphQLNonNull(GraphQLID) },
        payoutId: { type: GraphQLString }
    })
})

const ChargeType = new GraphQLObjectType({
    name: 'ChargeType',
    fields: () => ({
        _id: { type: GraphQLNonNull(GraphQLID) },
        chargeId: { type: GraphQLString }
    })
})

const TransactionHistoryType = new GraphQLObjectType({
    name: 'TransactionHistoryType',
    fields: () => ({
        _id: { type: GraphQLNonNull(GraphQLID) },
        userId: { type: GraphQLString },
        transactionId: { type: GraphQLString },
        customerId: { type: GraphQLString }
    })
})

const DisputeType = new GraphQLObjectType({
    name: 'DisputeType',
    fields: () => ({
        _id: { type: GraphQLNonNull(GraphQLID) },
        disputeId: { type: GraphQLString },
        chargeId: { type: GraphQLString }
    })
})

const RefundType = new GraphQLObjectType({
    name: 'RefundType',
    fields: () => ({
        _id: { type: GraphQLNonNull(GraphQLID) },
        refundId: { type: GraphQLString },
        chargeId: { type: GraphQLString }
    })
})

const CouponType = new GraphQLObjectType({
    name: 'CouponType',
    fields: () => ({
        _id: { type: GraphQLNonNull(GraphQLID) },
        couponId: { type: GraphQLString }
    })
})

const TransactionType = new GraphQLObjectType({
    name: 'TransactionType',
    fields: () => ({
        _id: { type: GraphQLNonNull(GraphQLID) },
        dateCreated: { type: GraphQLString },
        dateUpdated: { type: GraphQLString },
        userId: { type: GraphQLString },
        customerId: { type: GraphQLString },
        balance: { type: GraphQLInt },
        accounts: { type: GraphQLList(AccountType) },
        cardTokens: { type: GraphQLList(CardTokenType) },
        bankAccounts: { type: GraphQLList(BankAccountType) },
        payouts: { type: GraphQLList(PayoutType) },
        charges: { type: GraphQLList(ChargeType) },
        transactions: { type: GraphQLList(TransactionHistoryType) },
        disputes: { type: GraphQLList(DisputeType) },
        refunds: { type: GraphQLList(RefundType) },
        coupons: { type: GraphQLList(CouponType) },
        user: {
            type: UserType,
            resolve: async ({ userId }) => {
                const user = await userService.getUser(userId)
                return user
            }
        },
        activity: {
            type: ActivityType,
            resolve: async ({ _id }) => {
                const activity = await activityService.getActivityByTransactionId(_id)
                return activity
            }
        }
    })
})

const OtherType = new GraphQLObjectType({
    name: 'OtherType',
    fields: () => ({
        _id: { type: GraphQLNonNull(GraphQLID) },
        name: { type: GraphQLString },
        type: { type: GraphQLString },
    })
})

const CustomerServiceMessageType = new GraphQLObjectType({
    name: 'customerServiceMessageType',
    fields: () => ({
        _id: { type: GraphQLNonNull(GraphQLID) },
        title: { type: GraphQLString },
        message: { type: GraphQLString },
        dateCreated: { type: GraphQLString },
    })
})

const ActivityListContentType = new GraphQLObjectType({
    name: 'ActivityListContentType',
    fields: () => ({
        _id: { type: GraphQLNonNull(GraphQLID) },
        title: { type: GraphQLString },
        content: { type: GraphQLString },
        dateCreated: { type: GraphQLString }
    })
})

const RequestType = new GraphQLObjectType({
    name: 'RequestType',
    fields: () => ({
        _id: { type: GraphQLNonNull(GraphQLID) },
        title: { type: GraphQLString },
        content: { type: GraphQLString },
        accepted: { type: GraphQLBoolean },
        amountRequested: { type: GraphQLInt },
        userId: { type: GraphQLString },
        user: {
            type: UserType,
            resolve: async ({ userId }) => {
                const user = await userService.getUser(userId)
                return user
            }
        },
        settlementDate: { type: GraphQLString },
        dateCreated: { type: GraphQLString },
        dateUpdated: { type: GraphQLString }
    })
})

const ActivityType = new GraphQLObjectType({
    name: 'ActivityType',
    fields: () => ({
        _id: { type: GraphQLNonNull(GraphQLID) },
        userId: { type: GraphQLString },
        user: {
            type: UserType,
            resolve: async ({ userId }) => {
                const user = await userService.getUser(userId)
                return user
            }
        },
        transactionId: { type: GraphQLString },
        transaction: {
            type: TransactionType,
            resolve: async ({ transactionId }) => {
                const transaction = await transactionService.getTransaction(transactionId)
                return transaction
            }
        },
        amountRequested: { type: GraphQLInt },
        requests: { type: GraphQLList(RequestType) },
        customerServiceMessages: { type: GraphQLList(CustomerServiceMessageType) },
        userActivity: { type: GraphQLList(ActivityListContentType) },
        transactionActivity: { type: GraphQLList(ActivityListContentType) },
        accepted: { type: GraphQLBoolean },
        resolved: { type: GraphQLBoolean },
        dateCreated: { type: GraphQLString },
        dateUpdated: { type: GraphQLString }
    })
})

const UserType = new GraphQLObjectType({
    name: 'UserType',
    fields: () => ({
        _id: { type: GraphQLNonNull(GraphQLID) },
        firstName: { type: GraphQLString },
        lastName: { type: GraphQLString },
        userName: { type: GraphQLString },
        address: { type: GraphQLString },
        postalCode: { type: GraphQLString },
        age: { type: GraphQLString },
        phoneNumber: { type: GraphQLString },
        customerId: { type: GraphQLString },
        socialInsuranceNumber: { type: GraphQLString },
        sex: { type: GraphQLString },
        email: { type: GraphQLString },
        password: { type: GraphQLString },
        avatar: { type: GraphQLString },
        rating: { type: GraphQLString },
        accountType: { type: GraphQLString },
        transactionId: { type: GraphQLString },
        transaction: {
            type: TransactionType,
            resolve: async ({ transactionId }) => {
                const transaction = await transactionService.getTransaction(transactionId)
                return transaction
            }
        },
        activity: {
            type: ActivityType,
            resolve: async ({ _id }) => {
                const activity = await activityService.getActivityByUserId(_id)
                return activity
            }
        },
        token: { type: GraphQLString },
        verified: { type: GraphQLBoolean },
        other: { type: new GraphQLList(OtherType) },
        dateCreated: { type: GraphQLString },
        dateUpdated: { type: GraphQLString }
    })
})

const LoginType = new GraphQLObjectType({
    name: 'LoginType',
    fields: () => ({
        user: { type: UserType },
        accessToken: { type: GraphQLString },
        message: { type: GraphQLString },
        status: { type: GraphQLInt }
    })
})

const DefaultResponseType = new GraphQLObjectType({
    name: 'DefaultResponseType',
    fields: () => ({
        message: { type: GraphQLString },
        user: { type: UserType },
        status: { type: GraphQLInt }
    })
})


const ResponseType = new GraphQLObjectType({
    name: 'ResponseType',
    fields: () => ({
        message: { type: GraphQLString },
        accessToken: { type: GraphQLString },
        status: { type: GraphQLInt }
    })
})

const ActivityResponseType = new GraphQLObjectType({
    name: 'ActivityResponseType',
    fields: () => ({
        message: { type: GraphQLString },
        activity: { type: GraphQLString },
        status: { type: GraphQLInt }
    })
})

const TransactionResponseType = new GraphQLObjectType({
    name: 'TransactionResponseType',
    fields: () => ({
        message: { type: GraphQLString },
        transaction: { type: TransactionType },
        status: { type: GraphQLInt }
    })
})

module.exports = { TransactionType, UserType, LoginType, ResponseType, DefaultResponseType, TransactionResponseType, ActivityType, ActivityResponseType }