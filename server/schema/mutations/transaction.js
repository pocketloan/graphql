const { TransactionResponseType, DefaultResponseType } = require('../types')
const { GraphQLString, GraphQLID, GraphQLNonNull, GraphQLInt } = require('graphql')
const GraphQLJSON = require('graphql-type-json')
const TransactionService = require('../../services/transaction')

const transactionService = new TransactionService()

module.exports = {
    updateTransaction: {
        type: TransactionResponseType,
        args: {
            userId: { type: GraphQLNonNull(GraphQLID) },
            customerId: { type: GraphQLString },
            balance: { type: GraphQLInt },
            cardTokens: {
                type: GraphQLJSON,
                description: "Field expected is a string `cardToken`"
            },
            created: { type: GraphQLString },
            clientIp: { type: GraphQLString },
            bankAccounts: {
                type: GraphQLJSON,
                description: "Field expected is a string `bankAccountToken`"
            },
        },
        resolve: async (_, args) => {
            const transaction = await transactionService.updateTransaction(args)
            return transaction
        }
    },
    updateMandatoryFields: {
        type: DefaultResponseType,
        args: {
            userId: { type: GraphQLNonNull(GraphQLID) },
            accountId: { type: GraphQLNonNull(GraphQLID) },
            dob: { type: GraphQLString },
            socialInsuranceNumber: { type: GraphQLString },
            address: { type: GraphQLString },
            state: { type: GraphQLString },
            city: { type: GraphQLString },
            postalCode: { type: GraphQLString }
        },
        resolve: async (_, args) => {
            const updated = await transactionService.updateMandatoryFields(args)
            return updated
        }
    }
}