const { GraphQLString, GraphQLNonNull, GraphQLID, GraphQLBoolean } = require('graphql')
const { LoginType, ResponseType, DefaultResponseType } = require('../types')
const UserService = require('../../services/user')

const userService = new UserService()

module.exports = {
    createUser: {
        type: ResponseType,
        args: {
            firstName: { type: GraphQLString },
            lastName: { type: GraphQLString },
            email: { type: GraphQLString },
            userName: { type: GraphQLString },
            password: { type: GraphQLString },
            sex: { type: GraphQLString },
            accountType: { type: GraphQLString }
        },
        resolve: async (_, args) => {
            const newAccount = await userService.createUser(args)
            return newAccount
        }
    },
    login: {
        type: LoginType,
        args: {
            field: {
                description: `User's username or email address`,
                type: GraphQLString
            },
            password: { type: GraphQLString }
        },
        resolve: async (_, { field, password }) => {
            const loginResponse = await userService.login({ field, password })
            return loginResponse
        }
    },
    updateUser: {
        type: DefaultResponseType,
        args: {
            id: { type: GraphQLNonNull(GraphQLID) },
            firstName: { type: GraphQLString },
            lastName: { type: GraphQLString },
            userName: { type: GraphQLString },
            address: { type: GraphQLString },
            postalCode: { type: GraphQLString },
            age: { type: GraphQLString },
            phoneNumber: { type: GraphQLString },
            sex: { type: GraphQLString },
            email: { type: GraphQLString },
            password: { type: GraphQLString },
            avatar: { type: GraphQLString },
            rating: { type: GraphQLString },
            accountType: { type: GraphQLString },
            token: { type: GraphQLString },
            verified: { type: GraphQLBoolean }
        },
        resolve: async (_, args) => {
            const updateResponse = await userService.updateUser(args)
            return updateResponse
        }
    },
    verifyUser: {
        type: DefaultResponseType,
        args: {
            id: { type: GraphQLNonNull(GraphQLID) }
        },
        resolve: async (_, args) => {
            const verifyResponse = await userService.verifyUser(args)
            return verifyResponse
        }
    }
}