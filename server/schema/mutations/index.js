const { GraphQLObjectType } = require('graphql')
const { 
    login, 
    createUser, 
    updateUser, 
    verifyUser 
} = require('./user')
const { 
    updateTransaction, 
    updateMandatoryFields 
} = require('./transaction')
const { dispatchRequest } = require('./activity')

const mutation = new GraphQLObjectType({
    name: 'mutation',
    fields: () => ({ 
        login, 
        createUser, 
        updateUser, 
        verifyUser, 
        updateTransaction,
        updateMandatoryFields,
        dispatchRequest
    })
})

module.exports = mutation