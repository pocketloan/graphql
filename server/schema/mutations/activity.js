const { GraphQLID, GraphQLNonNull, GraphQLString, GraphQLInt } = require('graphql')
const { ActivityResponseType } = require('../types')
const ActivityService = require('../../services/activity')

const activityService = new ActivityService()


module.exports = {
    dispatchRequest: {
        type: ActivityResponseType,
        args: {
            userId: { type: GraphQLNonNull(GraphQLID) },
            content: { type: GraphQLString },
            title: { type: GraphQLString },
            audience: { type: GraphQLString },
            amountRequested: { type: GraphQLInt },
            settlementDate: {
                type: GraphQLString,
                description: 'Expected date for a particular transaction to be concluded'
            }
        },
        resolve: async (_, args) => {
            const dispatchRequest = await activityService.dispatchRequest(args)
            return dispatchRequest
        }
    },
}