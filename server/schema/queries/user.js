const { GraphQLList, GraphQLID, GraphQLNonNull, GraphQLString } = require('graphql')
const { UserType } = require('../types')
const UserService = require('../../services/user')

const userService = new UserService()

module.exports = {
    users: {
        type: GraphQLList(UserType),
        args: { 
            firstName: { type: GraphQLString },
            lastName: { type: GraphQLString },
            userName: { type: GraphQLString },
            email: { type: GraphQLString },
            sex: { type: GraphQLString },
            age: { type: GraphQLString },
            rating: { type: GraphQLString },
            verified: { type: GraphQLString },
            accountType: { type: GraphQLString }
        },
        resolve: async (_, args) => {
            const users = await userService.getUsers(args)
            return users
        }
    },
    user: {
        type: UserType,
        args: { id: { type: GraphQLNonNull(GraphQLID) } },
        resolve: async (_, { id }) => {
            const user = await userService.getUser(id)
            return user
        }
    }
}