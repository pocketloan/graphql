const { GraphQLObjectType } = require('graphql')
const {
    users,
    user
} = require('./user')
const {
    transactions,
    transaction,
    charges,
    charge,
    customers,
    customer,
    balances,
    balance,
    payouts,
    payout,
    accounts,
    account
} = require('./transaction')
const { activities, activity } = require('./activity')

const query = new GraphQLObjectType({
    name: 'query',
    fields: () => ({
        users,
        user,
        transactions,
        transaction,
        charges,
        charge,
        customers,
        customer,
        balances,
        balance,
        payouts,
        payout,
        accounts,
        account,
        activities,
        activity
    })
})

module.exports = query