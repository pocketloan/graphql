const { GraphQLList, GraphQLID } = require('graphql')
const GraphQLJSON = require('graphql-type-json')
const { TransactionType } = require('../types')
const TransactionService = require('../../services/transaction')

const transactionService = new TransactionService()

module.exports = {
    transactions: {
        type: GraphQLList(TransactionType),
        resolve: async () => {
            const transactions = await transactionService.getTransactions()
            return transactions
        }
    },
    transaction: {
        type: TransactionType,
        args: { id: { type: GraphQLID } },
        resolve: async (_, { id }) => {
            const transaction = await transactionService.getTransaction(id)
            return transaction
        }
    },
    charges: {
        type: GraphQLList(GraphQLJSON),
        resolve: async () => {
            const charges = await transactionService.getCharges()
            return charges
        }
    },
    charge: {
        type: GraphQLJSON,
        args: { id: { type: GraphQLID } },
        resolve: async (_, { id }) => {
            const charge = await transactionService.getCharge(id)
            return charge
        }
    },
    customers: {
        type: GraphQLList(GraphQLJSON),
        resolve: async () => {
            const customers = await transactionService.getCustomers()
            return customers
        }
    },
    customer: {
        type: GraphQLJSON,
        args: { id: { type: GraphQLID } },
        resolve: async (_, { id }) => {
            const customer = await transactionService.getCustomer(id)
            return customer
        }
    },
    balances: {
        type: GraphQLList(GraphQLJSON),
        resolve: async () => {
            const balances = await transactionService.getBalances()
            return balances
        }
    },
    balance: {
        type: GraphQLJSON,
        resolve: async () => {
            const balance = await transactionService.getBalance()
            return balance
        }
    },
    payouts: {
        type: GraphQLList(GraphQLJSON),
        resolve: async () => {
            const payouts = await transactionService.getPayouts()
            return payouts
        }
    },
    payout: {
        type: GraphQLJSON,
        args: { id: { type: GraphQLID } },
        resolve: async (_, { id }) => {
            const payout = await transactionService.getPayout(id)
            return payout
        }
    },
    accounts: {
        type: GraphQLList(GraphQLJSON),
        resolve: async () => {
            const accounts = await transactionService.getAccounts()
            return accounts
        }
    },
    account: {
        type: GraphQLJSON,
        args: { id: { type: GraphQLID } },
        resolve: async (_, { id }) => {
            const account = await transactionService.getAccount(id)
            return account
        }
    },
}