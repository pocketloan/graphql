const { GraphQLList, GraphQLID, GraphQLNonNull } = require('graphql')
const { ActivityType } = require('../types')
const ActivityService = require('../../services/activity')

const activityService = new ActivityService()

module.exports = {
    activities: {
        type: GraphQLList(ActivityType),
        resolve: async (_, args) => {
            const users = await activityService.getActivities()
            return users
        }
    },
    activity: {
        type: ActivityType,
        args: { id: { type: GraphQLNonNull(GraphQLID) } },
        resolve: async (_, { id }) => {
            const activity = await activityService.getActivityById(id)
            return activity
        }
    }
}