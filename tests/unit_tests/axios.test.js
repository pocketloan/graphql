require('dotenv').config()
const assert = require('assert')
const axios = require.main.require('server/utils/axios')

describe('Axios', () => {
    describe('setAuthHeaders', () => {
        it('should return an object of the axios dependency', async () => {
            assert.equal(typeof axios, 'function')
        })
    })
})
